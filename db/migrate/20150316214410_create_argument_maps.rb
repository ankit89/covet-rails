class CreateArgumentMaps < ActiveRecord::Migration
  def change
    create_table :argument_maps do |t|
      t.string :name
      t.string :map_ds
      t.references :project

      t.timestamps null: false
    end
  end
end
