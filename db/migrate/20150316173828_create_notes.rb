class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|

      t.references :project
      t.references :user
      t.string "document_name"
      t.string "annotation_index_json"

      t.timestamps null: false
    end
  end
end
