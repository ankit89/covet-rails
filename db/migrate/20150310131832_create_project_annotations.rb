class CreateProjectAnnotations < ActiveRecord::Migration
  def change
    create_table :project_annotations do |t|

    	t.references :project
    	t.text :annotation_data

    	t.timestamps null: false
    end
  end
end
