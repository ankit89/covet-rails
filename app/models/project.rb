class Project < ActiveRecord::Base
	belongs_to :user

	has_many :memberships
	has_many :users, through: :memberships

	has_many :notes
	has_many :argument_maps

	has_one :project_annotations
end
