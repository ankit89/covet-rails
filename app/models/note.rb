class Note < ActiveRecord::Base
	belongs_to :project
	belongs_to :user
  
  validates :project_id, presence: true
  validates :user_id, presence: true
  
  validates :document_name, presence: true, allow_blank: false
  validates :annotation_index_json, presence: true, allow_blank: false
  validates :content, presence: true, allow_blank: false
  
end
