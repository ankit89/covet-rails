class ProjectsController < ApplicationController
	def new
		@project = Project.new
	end

	def create
		@project = Project.new(project_params)
		@project.user = current_user
		@project.users << current_user
		@project.save
		redirect_to root_url
	end

	def show
		@project = Project.find(params[:id])
		@notes = @project.notes
		@arg_maps = @project.argument_maps
		@files = Dir.entries("#{Rails.root}/public/data")
		@users = @project.users
    @new_note = Note.new
	end

	def list
	end

	def update
		project = Project.find(params[:id])
		if params[:project]
			if params[:project][:name] && !(params[:project][:name].empty?)
				project.name = params[:project][:name]
			end

			if params[:project][:user_email]
				user = User.find_by_email(params[:project][:user_email])
				if user != nil
					project.users << user
				end
			end
			project.save
		end

		redirect_to :back
	end

	def settings
		@project = Project.find(params[:id])
	end

	def update_annotation
		@project = Project.find(params[:id])
		redirect_to project_path(@project)
	end

	private 
	def project_params
		params.require(:project).permit(:name)
	end
end
