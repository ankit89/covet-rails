class ArgumentMapsController < ApplicationController

	def create
		respond_to do |format|
			format.js {
				@amap = ArgumentMap.new
				@amap.name = params[:name]
				@amap.map_ds = params[:map_ds]
				@amap.project = Project.find(params[:project_id])
				@amap.save
			}
		end
	end

	def update
		respond_to do |format|
			format.js{
				@amap = ArgumentMap.find(params[:id])
				@amap.name = params[:name]
				@amap.map_ds = params[:map_ds]
				@amap.save
			}
		end
	end
end
