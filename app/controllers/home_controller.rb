class HomeController < ApplicationController

	def index
		if !user_signed_in? then
			redirect_to new_user_session_path
		else
			@projects = current_user.projects
		end
	end
end
