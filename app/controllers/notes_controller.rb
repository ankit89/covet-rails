class NotesController < ApplicationController

	def create
		respond_to do |format|
			format.js {
				@note = Note.create(note_params)
				@note.user = current_user
				@success = @note.save
			}
		end
	end

	private
	def note_params
		params.require(:note).permit(:content, :project_id, :document_name, :annotation_index_json)
	end
end
