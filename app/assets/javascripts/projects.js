/* global require, d3, ace, Infinity */

// Global variables
var Range = require('ace/range').Range;

/**
 * This is the list of all Ace editor marker ids
 * @type {Array}
 */
var allMarkers = [];

/**
 * TODO: Add description of selectedMapNodes
 * @type {Array}
 */
var selectedMapNodes = [];

/**
 * The id of the current project is setup by the erb file.
 * @type Number
 */
var project_id;
/**
 * This is the list of all the documents loaded for the project
 * @type Array
 */
var docs = [];

/**
 * This is a placeholder array for all documents when updating docs
 * @type Array
 */
var allDocsBackup = [];

/**
 * TODO add description for the docMap object
 */
var docMap = {};

/**
 * TODO: add description
 * @type type
 */
var selectedNoteMarkerId;

/**
 * TODO: add description
 * @type Array
 */
var notes = [];

var editor;

/**
 * TODO: add description
 * @type object
 */
var docListIndexMap = {};

/**
 * TODO: add description
 * @type type
 */
var current_author;

/**
 * lunr index for reports
 * @type type
 */
var index = lunr(function () {
    this.field('reportId', {boost: 10});
    this.field('reportFullText');
    this.ref('reportId');
});

/**
 * lunr index for notes
 * @type type
 */
var noteIndex = lunr(function () {
    this.field('id', {boost: 10});
    this.field('content');
    this.ref('id');
});

var selectedMapNodes = [];

var selectedMap;

var author_email_map = {};

//TODO: add description
var arg_maps = [];
var arg_map_names = [];

/**
 * This method is called when a note is successfully added
 * @param note a json object representing the newly added note.
 */
function new_note_added(note) {
    $("#note-dlg").dialog('close');
    updateNotes([note], true);

    // highlight the new note
    var range = JSON.parse(note.annotation_index_json);
    var notehighlight = findAuthorColor(note.user_id);
    console.log(range);
    console.log("highlight: " + notehighlight);
    
    var markerId = editor.session.addMarker(new Range(range.start.row, range.start.column, range.end.row, range.end.column),
            notehighlight, "text");
    console.log("editor: " + editor);
    allMarkers.push(markerId);

    updateNoteCounts();
    $("#docList").jqxListBox("refresh");
}

/**
 * Updates the notes model and corresponding UI.
 * @param {Array} new_notes array of notes to be added
 * @param {Boolean} appendToExisting remove existing notes, if false. Otherwise, append.
 * @returns {undefined}
 */
function updateNotes(new_notes, appendToExisting) {
    if (!appendToExisting) {
        // empty the notes array
        notes.length = 0;
        //TODO: purge other data structures containing notes.
    }

    for (var n in new_notes) {
        var note = new_notes[n];
        notes.push(note);
        var style = findAuthorStyle(note.user_id);

        $("<li data-note-id='" + note.id + "' data-doc='" + note.document_name + "' style='" + style + "'>" +
                "<span class='note-author'>" + findUserEmailById(note.user_id) + "</span>&nbsp;&nbsp;<span><b>" + note.created_at + "</b></span><br/>" +
                "<div>" + note.content + "</div>" +
                "</li>").appendTo("#doc-note-list");
        noteIndex.add(note);
        addNoteDiv(note);

    }
}

/**
 * Returns the email of a user from his/her id
 * @returns {String}
 */
function findUserEmailById(id) {
    console.log(id + " " + author_email_map[id]);
    return author_email_map[id];
}

/**
 * This is a method that is called when a new note creation fails
 * @param {hash} error error messages if the note addition failed.
 */
function note_addition_failed(error) {
    console.log("note cannot be created because " + error);
    $("#note-dlg").dialog('close');
}

/**
 * Initializes the Ace editor at a given div
 * @param  {String} editorId of the div
 * @return the editor javascript object
 */
function initializeEditor(editorId) {
    editor = ace.edit(editorId);
    editor.session.setUseWrapMode(true);
    editor.$blockScrolling = Infinity;
    return editor;
}

/**
 * This function calls the UI initializing code
 */
function setupUI() {
    //create tabs
    $("#tabs").tabs();
//    $("#tabs").tabs("option", "active", 1);

    $("#note-dlg").dialog({
        autoOpen: false,
        hide: "toggle",
        width: "auto",
        height: "auto",
        closeOnEscape: true,
        resize: "auto",
        modal: true,
        title: "Add a note"
    });

    $("#search-note-dialog").dialog({
        "autoOpen": false
    });
}

/**
 * This function adds event handlers for different UI components
 * @param {object} params parameters to be used when setting up event handlers
 */
function setupEventHandlers(params) {

    var editor = params.editor;

    // Event handler for when the user clicks the note button to create add a note to a document
    $("#note-btn").on("click", function () {
        var range = editor.selection.getRange();
        $("input[name=note\\[annotation_index_json\\]]").val(JSON.stringify(range));

        // var markerId = editor.session.addMarker(new Range(range.start.row, range.start.column, range.end.row, range.end.column),
        //         "highlight", "text");
        // allMarkers.push(markerId);

        $('#note-dlg').dialog("open");

        return false;
    });

    // Add event handler for when the user selects a document from the document list
    $("#docList").on("select", function (event) {
        var args = event.args;
        if (args) {
            var item = args.item;
            var current_document_name = item.value;
            $("input[name=note\\[document_name\\]]").val(current_document_name);

            $("#doc-note-list").find("li").hide();
            $("#doc-note-list").find("li[data-doc='" + current_document_name + "']").show();
            if (typeof selectedNoteMarkerId !== 'undefined') {
                editor.session.removeMarker(selectedNoteMarkerId);
            }

            //remove previous markers
            for (i in allMarkers) {
                editor.session.removeMarker(allMarkers[i]);
            }

            var selectedObject = item.originalItem;
            var docContent = selectedObject.reportFullText;
            var entities = selectedObject.entities;

            for (var n in notes) {
                if (notes[n].document_name === current_document_name) {
                    var range = JSON.parse(notes[n].annotation_index_json);
                    var notehighlight = findAuthorColor(notes[n].user_id);

                    var markerId = editor.session.addMarker(new Range(range.start.row, range.start.column, range.end.row, range.end.column),
                            notehighlight, "text");
                    allMarkers.push(markerId);
                }
            }

            editor.setValue(docContent, -1);

            for (var type in entities) {
                var ents = entities[type];

                //if there is a single entity and the json does 
                //not have an array for single entity, then convert into one
                if (typeof (ents) === "string") {
                    ents = [ents];
                }

                for (var i in ents) {
                    var e = ents[i];
                    //find the index of each entity
                    var length = e.length;
                    //this line will update the editor model to contain all selections
                    var entityCount = editor.findAll(e);

                    var ranges = editor.getSelection().getAllRanges();
                    for (var i in ranges) {
                        var range = ranges[i];
                        var markerId = editor.session.addMarker(new Range(range.start.row, range.start.column, range.end.row, range.end.column),
                                type, "text");
                        allMarkers.push(markerId);
                    }
                }
            }
        }
    });

    // Event handler for when the user clicks on argument map button to create a new argument map.
    $("#argument-map-btn").on("click", function () {
        var svg = d3.select("svg");
        var mapName = window.prompt("Please enter a name", "Untitled");

        if (!mapName) {
            return;
        }

        $.ajax({
            "url": "/argument_maps",
            "method": "post",
            "data": {
                name: mapName,
                map_ds: JSON.stringify({
                    "name": "Hypothesis: " + mapName,
                    when: strftime('%d-%b-%y')
                }),
                project_id: project_id
            }
        });
    });

    // Global event listener for click event on "more-link" for document in argument view.
    $(document).on("click", ".more-link", function (e) {
        var docId = $(this).data("doc-id");
        for (var d in docs) {
            var doc = docs[d];
            if (doc.reportId === docId) {
                $("#tabs").tabs("option", "active", 0);
                var items = $("#docList").jqxListBox("getItems");
                for (var i in items) {
                    if (items[i].originalItem.reportId == doc.reportId) {
                        $("#docList").jqxListBox("selectItem", items[i]);
                        break;
                    }
                }
                break;
            }
        }
    });

    // Global listener for keyboard keyup event.
    $(document).on("keyup", function (e) {
        var selected = d3.select("rect.selected");
        if (selected[0][0] !== null) {
            switch (e.keyCode) {
                case 65:
                    node_attach_requested();
                    break;
                case 68:
                    node_delete_requested();
                    break;
            }
        }
    });

    // Event handler that is called as the user types in the search box
    $("#search-terms").on("keydown", function () {

        filter_notes($("#search-terms").val());
    });

    // when the content of the search box changes
    $("#search-box").on("change", function (event) {
        //the user presses enter key

        var value = $("#search-box").val();
        if (value === "") {
            //load all the documents
            docs.splice(0);
            for (var i in allDocsBackup) {
                docs.push(allDocsBackup[i]);
            }
        } else {
            var searchResults = index.search(value);
            var searchIdArray = [];

            for (var i in searchResults) {
                searchIdArray.push(searchResults[i].ref);
            }
            docs.splice(0);
            for (var i in allDocsBackup) {
                if (searchIdArray.indexOf(allDocsBackup[i].reportId) > -1) {
                    docs.push(allDocsBackup[i]);
                }
            }
        }
        $("#docList").jqxListBox('refresh');
    });

    // event handler for when the search terms change
    $("#search-terms").on("change", function () {
        var text = $("#search-terms").val();

        if (text === "") {
            //show all notes
            $("#notes").children().show();
        } else {
            var notes = noteIndex.search(text);
            $("#notes").children().hide();
            for (var n in notes) {
                var ref = notes[n].ref;
                var noteId = "#n" + ref;
                $(noteId).show();
            }
            console.log(notes);
        }
    });

    $("ul").on("click", "li[data-note-id]", function () {
        var selectedNoteId = $(this).data("note-id");

        for (var n in notes) {
            if (notes[n].id === selectedNoteId) {
                var annotationRange = JSON.parse(notes[n].annotation_index_json);
                var r = new Range(annotationRange.start.row, annotationRange.start.column, annotationRange.end.row, annotationRange.end.column);

                if (typeof selectedNoteMarkerId !== 'undefined') {
                    editor.session.removeMarker(selectedNoteMarkerId);
                }
                selectedNoteMarkerId = editor.session.addMarker(r, "note-select", "text");
            }
        }
    });
}
//End of setupEventHandlers()

/**
 * This is the function that will be called on dom load and page:load(turbolinks) events.
 */
function ready() {
    //create the editor
    var editorId = "editor";
    var editor;
    if ($("#" + editorId).length === 1) {
        editor = initializeEditor(editorId);
    } else {
        console.warn("Could not find editor");
    }

    setupUI();

    setupEventHandlers({editor: editor});
}
// end of ready function

$(document).ready(ready);
$(document).on('page:load', ready);


function showDocsInSourceList(reportId) {
    if (!reportId) {
        for (var d in docs) {
            $("#arg-view-source-list")
                    .append("<li>" + docs[d].reportFullText + "<br/><a class='more-link' data-doc-id='" + docs[d].reportId + "' href='#'>show in document view</a></li>");
        }
    } else {
        for (d in docs) {
            if (docs[d].reportId === reportId) {
                $("#arg-view-source-list")
                        .append("<li>" + docs[d].reportFullText + "<br/><a class='more-link' data-doc-id='" + docs[d].reportId + "' href='#'>show in document view</a></li>");
            }
        }
    }
}

function filter_notes(text) {

}

function findDocNameForNoteId(noteId) {
    for (var n in notes) {
        if (notes[n].id === noteId) {
            return notes[n].document_name;
        }
    }
}

function updateArgumentMap() {
    var url = "/argument_maps/" + selectedMap.id;
    //removing parent references to prevent circular links
    for (var i in selectedMapNodes) {
        selectedMapNodes[i].parent = null;
    }
    $.ajax({
        url: url,
        method: "PUT",
        data: {
            "name": selectedMap.name,
            "map_ds": JSON.stringify(selectedMap.map_ds)
        }
    });
}


function node_attach_requested() {
    // Implement this
    // create a popup to allow the user to select a note
    // as +ve/-ve argument for the hypothesis

    var selected = d3.select("rect.selected");
    var nodeData = selected.data()[0];


    $("#search-note-dialog").dialog({
        autoOpen: false,
        width: 550,
        height: 447,
        modal: true,
        title: "Select argument",
        buttons: [{
                text: "Support",
                class: "positive",
                click: function () {
                    if (!nodeData.children) {
                        nodeData.children = [];
                    }
                    var noteId = +$("input[name=selected_note]:checked").parent().parent().data("note-id");
                    var child = {
                        name: $("input[name=selected_note]:checked").parent().siblings()[0].innerHTML,
                        support: "positive",
                        author: current_author,
                        note_id: noteId,
                        doc_id: findDocNameForNoteId(noteId),
                        when: strftime('%d-%b-%y')
                    };
                    nodeData.children.push(child);

                    drawArgumentMap(d3.select("svg"), nodeData);
                    $("#search-note-dialog").dialog('close');

                    updateArgumentMap();
                }
            }, {
                text: "Oppose",
                class: "negative",
                click: function () {
                    if (!nodeData.children) {
                        nodeData.children = [];
                    }
                    var noteId = +$("input[name=selected_note]:checked").parent().parent().data("note-id");
                    var child = {
                        name: $("input[name=selected_note]:checked").parent().siblings()[0].innerHTML,
                        support: "negative",
                        author: current_author,
                        note_id: noteId,
                        doc_id: findDocNameForNoteId(noteId),
                        when: strftime('%d-%b-%y')
                    };

                    nodeData.children.push(child);
                    drawArgumentMap(d3.select("svg"), nodeData);
                    $("#search-note-dialog").dialog('close');
                    updateArgumentMap();
                }
            }, {
                text: "Cancel",
                click: function () {
                    $("#search-note-dialog").dialog('close');
                }
            }]
    });
    $('button.positive').css('background', '#BCED91');
    $('button.negative').css('background', '#EE6363');
    $("#search-note-dialog").dialog('open');

}

function node_delete_requested() {

}

/**
 * Draw the argument map using the argument data
 * @param  svg the svg javascript object.
 * @param  data the data to be used for the argument map.
 */
function drawArgumentMap(svg, data) {

    svg.selectAll("*").remove();
    var tree = d3.layout.tree(data);
    nodes = tree.nodes(data),
            links = tree.links(nodes);

    for (var i in nodes) {
        nodes[i].y = nodes[i].depth * 180 + 120;
        nodes[i].x = nodes[i].x * 1000;
    }

    var diagonal = d3.svg.diagonal()
            .projection(function (d) {
                return [d.x, d.y];
            });

    var link = svg.selectAll("path")
            .data(links);

    source = data;
    source.x0 = pX(source);
    source.y0 = pY(source);
    // Enter any new links at the parent's previous position.
    link.enter().append("path")
            .attr("class", "link")
            .attr("d", function (d) {
                var o = {
                    x: source.x0,
                    y: source.y0
                };
                return diagonal({
                    source: o,
                    target: o
                });
            });

    var node = svg.selectAll(".node")
            .data(nodes)
            .enter().append("g")
            .attr("class", "node");

    var gNode = node.append("g");

    gNode.append("rect")
            .attr({
                "width": 100,
                "height": 75,
                "x": pX,
                "y": pY,
                "class": "arg-box"
            })
            .classed({
                "positive": function (d) {
                    return d.support === "positive";
                },
                "negative": function (d) {
                    return d.support === "negative";
                }
            })
            .on("dblclick", function (d) {
                var newText = window.prompt("Enter text", d.name);
                if (newText) {
                    d.name = newText;
                    $.ajax({
                        url: "/argument_maps/" + selectedMap.id,
                        method: "PUT",
                        data: {
                            "name": selectedMap.name,
                            "map_ds": JSON.stringify(selectedMap.map_ds)
                        }
                    });

                }
            })
            .on("click", function (d) {
                d3.select(this).classed({
                    "selected": !d3.select(this).classed("selected")
                });
            });

    gNode.append("rect")
            .attr({
                "width": 20,
                "height": 20,
                "x": function (d) {
                    return pX(d) + 84;
                },
                "y": function (d) {
                    return pY(d) + 59;
                },
                "class": "source",
                "fill": function (d) {
                    //return color based on the author
                    if (d.author === "joe@example.com") {
                        return "rgba(255,0,0,.5)";
                    } else if (d.author === "tom@example.com") {
                        return "rgba(0,255,0,.5)";
                    } else if (d.author === "harry@example.com") {
                        return "rgba(0,0,255,.5)";
                    }
                    return "#DDD";
                }
            })
            .style("cursor", "hand")
            .on("click", function (d) {
                var sourceDocId = d.doc_id;
                //clear sources if any
                $("#arg-view-source-list").html("");
                console.log(sourceDocId);
                showDocsInSourceList(sourceDocId);
            });

    // Transition links to their new position.
    link.transition()
            .duration(100)
            .attr("d", diagonal);

    function pX(d) {
        return d.x - 50;
    }

    function pY(d) {
        return d.y - 75 / 2;
    }


    node.append("text")
            .attr({
                "dy": ".9em",
                "x": pX,
                "y": pY
            })
            .text(function (d) {
                //also add the nodes to the selectedMapNodes
                selectedMapNodes.push(d);
                return d.name;
            });

    node.append("text")
            .attr({
                "dy": ".9em",
                "x": pX,
                "y": function (d) {
                    return pY(d) + 60;
                }
            })
            .style("font-size", 9)
            .style("font-weight", "bold")
            .text(function (d) {
                return d.when + "";
            });

    function wrap(text, width) {
        text.each(function () {
            var text = d3.select(this),
                    x = text.attr("x"),
                    words = text.text().split(/\s+/).reverse(),
                    word,
                    line = [],
                    lineNumber = 0,
                    lineHeight = 1.1, // ems
                    y = text.attr("y"),
                    dy = parseFloat(text.attr("dy")),
                    tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em").attr("dx", ".25em");
            while (word = words.pop()) {
                line.push(word);
                tspan.text(line.join(" "));
                if (tspan.node().getComputedTextLength() > width) {
                    line.pop();
                    tspan.text(line.join(" "));
                    line = [word];
                    tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").attr("dx", ".25em").text(word);
                }
            }
        });
    }

    wrap(svg.selectAll("text"), 100);
}

function getIndicesOf(searchStr, str, caseSensitive) {
    var startIndex = 0,
            searchStrLen = searchStr.length;
    var index, indices = [];
    if (!caseSensitive) {
        str = str.toLowerCase();
        searchStr = searchStr.toLowerCase();
    }
    while ((index = str.indexOf(searchStr, startIndex)) > -1) {
        indices.push(index);
        startIndex = index + searchStrLen;
    }
    return indices;
}

/**
 * Returns the css style associated with an author.
 * @param {Number} id the id of the
 */
function findAuthorStyle(id) {
    var email = author_email_map[id];
    switch (email) {
        case "joe@example.com":
            return "background: rgba(255,0,0,.5);";
        case "tom@example.com":
            return "background: rgba(0,255,0,.5);";
        case "harry@example.com":
            return "background: rgba(0,0,255,.5);";
    }
}

/**
 * Finds the color associated with an author
 * @param {Number} id the id of the collaborator
 */
function findAuthorColor(id) {
    var email = author_email_map[id];
    switch (email) {
        case "joe@example.com":
            return "rednotehighlight";
        case "tom@example.com":
            return "greennotehighlight";
        case "harry@example.com":
            return "bluenotehighlight";
        default:
            return "notehighlight";
    }
}