/**
* Visualizes words based on their weights
* @param containerDiv the id of the div where the visualization should be created
* @param words the array of words to be used for visualization, each element of the array is an object containining
*		the word value and weight
* @param params any additional parameters to be used for the visualization.
*/
function visualizeWords(containerDiv, words, params) {
	// sort the words
	var sortedWords = words.sort(function(a, b) {
		return b.weight - a.weight;
	});

	d3.select("#" + containerDiv)
		.append("ul")
		.selectAll("li")
		.data(sortedWords)
		.enter()
		.append("li")
		.text(function(d){
			return d.value;
		})
}