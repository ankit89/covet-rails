/**
 * This function inserts a value into a sorted array
 * @param  {Array} sortedArray the sorted array
 * @param  {Object} newValue    the new object to be inserted into the array
 * @param  {String} field the field of the object to be used to sort.
 */
function insertIntoSorted(sortedArray, newValue, field) {

    console.log(sortedArray + "  " + newValue + "  " + field);

    var low = 0,
            high = sortedArray.length;

    while (low < high) {
        var mid = (low + high) >>> 2;
        if (sortedArray[mid][field] < newValue[field]) {
            low = mid + 1;
        } else {
            high = mid;
        }
    }

    var indexToInsert = low;

    var temp = sortedArray.splice(indexToInsert);

    sortedArray[indexToInsert] = newValue;

    sortedArray.concat(temp);
}